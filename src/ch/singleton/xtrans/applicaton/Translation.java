package ch.singleton.xtrans.applicaton;

public class Translation {
    private String language;
    private String content;
    
    public Translation() {}
    
    public Translation(String language, String content) {
        this.language = language;
        this.content = content;
    }
    
    public String getLanguage() {
        return language;
    }
    public void setLanguage(String language) {
        this.language = language;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
}

