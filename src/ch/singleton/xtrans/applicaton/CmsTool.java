package ch.singleton.xtrans.applicaton;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.file.UploadedFile;

import ch.ivyteam.ivy.cm.ContentManagementSystem;
import ch.ivyteam.ivy.cm.ContentObject;
import ch.ivyteam.ivy.cm.ContentObjectType;
import ch.ivyteam.ivy.environment.Ivy;
import ch.singleton.xtrans.utils.CmsExportUtil;
import ch.singleton.xtrans.utils.CmsImportUtil;
import ch.singleton.xtrans.utils.CmsLocaleUtil;
import ch.singleton.xtrans.utils.CmsModifierUtil;
import ch.singleton.xtrans.utils.CmsTableFactory;
import ch.singleton.xtrans.utils.CollectionUtil;
import ch.singleton.xtrans.utils.FacesContextUtil;
import ch.singleton.xtrans.utils.ProjectLoaderUtil;
import ch.singleton.xtrans.utils.RequestContextUtil;
import ch.xpertline.xtrans.CMSObject;
import ch.xpertline.xtrans.CMSTranslator;
import ch.xpertline.xtrans.CmsTree;
import ch.xpertline.xtrans.TreeData;
import ch.xpertline.xtrans.excel.ExportHandler;
import ch.xpertline.xtrans.excel.ImportHandler;

@ManagedBean(name ="cmsTool")
@SessionScoped
public class CmsTool {
	
	private static String TABLE_COMPONENT = "form-content:cmsTable";

    private String projectName;
    private List<String> projectNames;
    private StreamedContent cmsStream;
    private TreeNode<TreeData> root;
    private TreeNode<TreeData> selectedNode;
    private List<ContentObject> contentObjectList;
    private List<CMSObject> cmsObjects;
    private List<CMSObject> cmsObjectsWithDifference;
    private CMSTranslator cmsTranslator;
    private boolean exportOnlyVisibleContentObjects;
    private UploadedFile file;
    private List<String> supportedLanguages;
    private List<String> availableLanguages;
    private String selectedLanguage;
    private String searchText;
    private CMSObject selectedCmsObject;
    private Translation selectedTranslation;
    private List<Translation> translations;
    private String content;

    private static final String DOWNLOAD_CMS = "PF('downloadCms').jq.click()";

    @PostConstruct
    public void init() {
        this.projectNames = ProjectLoaderUtil.buildProjectNameList();
        this.projectName = projectNames.stream().findFirst().orElse(StringUtils.EMPTY);
        try {
            this.cmsTranslator = this.loadTree();
            this.availableLanguages = CmsLocaleUtil.buildAvailableLanguageCodeAsStringList();
            this.loadTable();
        } catch (Exception e) {
            Ivy.log().error(e);
        }
    }
    
    private void loadTable() {
        this.supportedLanguages = CmsLocaleUtil.buildSupportedLanguageCodeList(this.cmsTranslator);
        this.cmsObjects = this.cmsTranslator.getWholeContent(this.contentObjectList);
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public List<String> getProjectNames() {
        return projectNames;
    }

    public void setProjectNames(List<String> projectNames) {
        this.projectNames = projectNames;
    }
    
    public void onChangeProjectName() throws Exception {
        Ivy.log().info("onChangeProjectName");
        this.cmsTranslator = this.loadTree();
        this.loadTable();
        FacesContextUtil.updateComponent("form-content:cmsTree");
        FacesContextUtil.updateComponent(TABLE_COMPONENT);
    }
    
    public void onNodeSelect() {
        updateCmsObjectsBySelectedNode(this.selectedNode);
    }
    
    private void updateCmsObjectsBySelectedNode(TreeNode<TreeData> selectedNode) {
        if(selectedNode != null) {
            this.cmsObjects = CmsTableFactory.buildTableByNode(selectedNode.getData().getUri(), this.cmsTranslator);
        }
        FacesContextUtil.updateComponent(TABLE_COMPONENT);
    }
    
    private CMSTranslator loadTree()  {
        this.root = null;
        CmsTree cmsTree = new CmsTree();
        CMSTranslator cmsTranslator = new CMSTranslator(this.projectName);
        ContentManagementSystem cms = cmsTranslator.getCms();
        if(cms == null) {
            Ivy.log().error("CMS can't be load -> check permission rights!");
        } else {
            this.contentObjectList = cmsTranslator.getContentHierarchy(cms.root());
            cmsTree.setData(this.contentObjectList);
            this.root = cmsTree.getTree();
        }
        return cmsTranslator;
    }
    
    public List<CMSObject> getCmsObjects() {
        return this.cmsObjects;
    }
    
    public TreeNode<TreeData> getRoot() {
        return this.root;
    }
    
    public TreeNode<TreeData> getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode<TreeData> selectedNode) {
        this.selectedNode = selectedNode;
    }
    
    public boolean isExportOnlyVisibleContentObjects() {
        return exportOnlyVisibleContentObjects;
    }

    public void setExportOnlyVisibleContentObjects(boolean exportOnlyVisibleContentObjects) {
        this.exportOnlyVisibleContentObjects = exportOnlyVisibleContentObjects;
    }
    
    public void onChangeExportOnlyVisibleContentObjects() {
        Ivy.log().info("Export only visible contnent objects: {0}", this.exportOnlyVisibleContentObjects);
    }
    
    public void prepareExportFile() throws Exception {
        Ivy.log().info("Export Excel File");
        if(this.exportOnlyVisibleContentObjects) {
            List<String> uriList = CollectionUtil.getOrEmpty(this.cmsObjects)
                    .stream()
                    .map(CMSObject::getPath)
                    .collect(Collectors.toList());
            Workbook workBook = ExportHandler.buildExcelExport(this.projectName, uriList, this.cmsTranslator);
            this.cmsStream = CmsExportUtil.buildStreamContent(workBook, this.projectName);
        } else {
            Workbook workBook = ExportHandler.buildFullExcelExport(this.projectName, this.root);
            this.cmsStream = CmsExportUtil.buildStreamContent(workBook, this.projectName);
        }
        RequestContextUtil.execute(DOWNLOAD_CMS);
    }
    
    public StreamedContent downloadExportedCms() {
        return this.cmsStream;
    }
    
    public void handleFileUpload(FileUploadEvent event) {
        UploadedFile file = event.getFile();
        if (file != null && file.getContent() != null && file.getContent().length > 0 && file.getFileName() != null) {
            this.file = file;
            Ivy.log().info("Upload file: {0}", this.file.getFileName());
            
            try {
                ImportHandler imporHandler = new ImportHandler(this.cmsTranslator);
                imporHandler.readExcelDataFromFile(this.file.getInputStream());
                if(this.exportOnlyVisibleContentObjects) {
                    this.cmsObjectsWithDifference = CmsImportUtil.extractObjectsWithDifferentValue(this.cmsObjects, imporHandler.getCmsObjectList());
                } else {
                    List<CMSObject> fulllCmsObjectsOfProject = CmsExportUtil.buildFullCmsObjectsForProject(this.cmsTranslator, this.supportedLanguages, this.root);
                    this.cmsObjectsWithDifference = CmsImportUtil.extractObjectsWithDifferentValue(fulllCmsObjectsOfProject, imporHandler.getCmsObjectList());
                }
                FacesContextUtil.updateComponent("form-content:cmsTableDifference");
            } catch (IOException e) {
                Ivy.log().error("Can not import excel data");
                return;
            }
            FacesContextUtil.addMessage(this.file.getFileName() + " is uploaded.");
        }
    }
    
    public List<CMSObject> getCmsObjectsWithDifference() {
        return cmsObjectsWithDifference;
    }

    public void setCmsObjectsWithDifference(List<CMSObject> cmsObjectsWithDifference) {
        this.cmsObjectsWithDifference = cmsObjectsWithDifference;
    }
    
    public boolean checkCmsDifferenceListEmpty() {
        return CollectionUtil.isNullOrEmpty(this.cmsObjectsWithDifference);
    }
    
    public void appendDifference() {
        Ivy.log().info("Append Modifications from imported excel");
        CollectionUtil.getOrEmpty(this.cmsObjectsWithDifference)
                .forEach(this::updateContentObjectByCms);
        FacesContextUtil.addMessage(String.format("CMS Modifications for project %s has been appended!", this.projectName));
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }
    
    public void search() {
        Ivy.log().info("Search text: {0}", this.searchText);
        List<CMSObject> cmsObjectList = CmsExportUtil.buildFullCmsObjectsForProject(this.cmsTranslator, this.supportedLanguages, this.root);
        this.cmsObjects = CollectionUtil.getOrEmpty(cmsObjectList)
                .stream()
                .filter(cmsObject -> StringUtils.containsAnyIgnoreCase(cmsObject.getText(), this.searchText))
                .collect(Collectors.toList());
        FacesContextUtil.updateComponent(TABLE_COMPONENT);
    }
    
    public CMSObject getSelectedCmsObject() {
        return selectedCmsObject;
    }

    public void setSelectedCmsObject(CMSObject selectedCmsObject) {
        this.selectedCmsObject = selectedCmsObject;
    }
    
    public void onSelectCmsObject() {
        Ivy.log().info("Row selected: {0}", this.selectedCmsObject.getPath());
    }
    
    public boolean shouldRenderEditIcon() {
        if(this.selectedCmsObject == null) {
            return true;
        }
        return selectedCmsObject.getType() == ContentObjectType.STRING;
    }
    
    public void setTranslations(List<Translation> translations) {
        this.translations = translations;
    }
    
    public List<Translation> getTranslations() {
        return this.translations;
    }
    
    public void extractTranslations() {
        if(this.selectedCmsObject == null) {
            return;
        }
        this.translations = this.selectedCmsObject.getValue().entrySet()
                .stream()
                .map(entry -> new Translation(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }
    
    public void updateTranslation(Translation translation) {
        if(this.selectedCmsObject == null) {
            return;
        }
        this.selectedCmsObject.getValue().put(translation.getLanguage(), translation.getContent());
        this.updateContentObjectByCms(this.selectedCmsObject);
    }
    
    public void onCellEdit(CellEditEvent<String> event) {
        String oldValue = event.getOldValue();
        String newValue = event.getNewValue();
        Translation translation = this.translations.get(event.getRowIndex());
        if (!StringUtils.equals(oldValue, newValue)) {
            this.updateTranslation(translation);
            updateCmsObjectsBySelectedNode(this.selectedNode);
            FacesContextUtil.addMessage(String.format("Cell Changed Old: %s, New: %s",  oldValue,newValue));
        }
    }
    
    public void updateContentObjectByCms(CMSObject cmsObject) {
        this.cmsTranslator.getCms().get(cmsObject.getPath())
        .ifPresent(contentObject -> {
            CmsModifierUtil.writeModifiedData(cmsObject, contentObject, this.supportedLanguages);
            CmsModifierUtil.updateCmsObjectsByCurrentSelected(this.cmsObjects, cmsObject);
        });
        FacesContextUtil.updateComponent(TABLE_COMPONENT);
    }
    
    public boolean failOnStartUp() {
        return this.cmsTranslator.getCms() == null;
    }
    
    public List<String> completeLanguage(String query) {
        String queryLowerCase = query.toLowerCase();
        this.availableLanguages.removeAll(this.supportedLanguages);
        return this.availableLanguages
                .stream()
                .filter(t -> t.toLowerCase().startsWith(queryLowerCase))
                .collect(Collectors.toList());
    }
    
    public String getSelectedLanguage() {
        return selectedLanguage;
    }

    public void setSelectedLanguage(String selectedLanguage) {
        this.selectedLanguage = selectedLanguage;
    }
    
    public void removeCmsObject() {
        Ivy.log().info("Remove CMS Object");
        this.translations.remove(this.selectedTranslation);
        this.selectedCmsObject.getValue().remove(this.selectedTranslation.getLanguage(), this.selectedTranslation.getContent());
        this.cmsTranslator.getCms().get(this.selectedCmsObject.getPath())
        .ifPresent(contentObject -> {
            CmsModifierUtil.deleteTranslation(contentObject, this.selectedLanguage);
            CmsModifierUtil.updateCmsObjectsByCurrentSelected(this.cmsObjects, this.selectedCmsObject);
            FacesContextUtil.updateComponent(TABLE_COMPONENT);
        });
    }
    
    public void addCmsObject() {
        Ivy.log().info("Add CMS Object");
        Translation newTranslation = new Translation(this.selectedLanguage.toUpperCase(), this.content);
        this.translations.add(newTranslation);
        this.selectedCmsObject.getValue().put(this.selectedLanguage.toUpperCase(), this.content);
        this.cmsTranslator.getCms().get(this.selectedCmsObject.getPath())
        .ifPresent(contentObject -> {
            CmsModifierUtil.addTranslation(contentObject, this.selectedLanguage, this.content);
            CmsModifierUtil.updateCmsObjectsByCurrentSelected(this.cmsObjects, this.selectedCmsObject);
            FacesContextUtil.updateComponent(TABLE_COMPONENT);
        });
    }
    
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
    public Translation getSelectedTranslation() {
        return selectedTranslation;
    }

    public void setSelectedTranslation(Translation selectedTranslation) {
        this.selectedTranslation = selectedTranslation;
    }

}

