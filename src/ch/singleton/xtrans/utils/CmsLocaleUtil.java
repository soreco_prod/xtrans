package ch.singleton.xtrans.utils;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import ch.xpertline.xtrans.CMSTranslator;

public final class CmsLocaleUtil {
    
    private CmsLocaleUtil() {}
    
    public static List<String> buildSupportedLanguageCodeList(CMSTranslator cmsTranslator) {
        if(cmsTranslator.getCms() == null) {
            return Collections.emptyList();
        }
        return CollectionUtil.getOrEmpty(cmsTranslator.getCms().locales())
                .stream()
                .map(Locale::getLanguage)
                .filter(StringUtils::isNotEmpty)
                .map(String::toUpperCase)
                .collect(Collectors.toList());
    }
    
    public static List<Locale> buildSupportedLocalesList(CMSTranslator cmsTranslator) {
        if(cmsTranslator.getCms() == null) {
            return Collections.emptyList();
        }
        return new ArrayList<>(cmsTranslator.getCms().locales());
    }
    
    public static List<Locale> buildAvailableLanguageCodeList() {
        return Arrays.asList(DateFormat.getAvailableLocales());
    }
    
    public static List<String> buildAvailableLanguageCodeAsStringList() {
        return CollectionUtil.getOrEmpty(Arrays.asList(DateFormat.getAvailableLocales()))
                .stream()
                .map(locale -> locale.toString())
                .filter(StringUtils::isNotEmpty)
                .collect(Collectors.toList());
    }

}
