package ch.singleton.xtrans.utils;

import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import ch.ivyteam.ivy.cm.ContentObject;
import ch.xpertline.xtrans.CMSObject;

public final class CmsModifierUtil {
    
    private CmsModifierUtil() {}
    
    public static void writeModifiedData(CMSObject cmsObject, ContentObject contentObject, List<String> supportedLanguages) {
        Set<String> keys = cmsObject.getValue().keySet();
        CollectionUtil.getOrEmpty(supportedLanguages)
                .stream()
                .filter(language -> keys.contains(language))
                .forEach(language -> {
                    contentObject.value()
                            .get(language.toLowerCase())
                            .write().string(cmsObject.getValue().get(language));
                });
    }
    
    public static void addTranslation(ContentObject contentObject, String language, String content) {
        contentObject.value()
                 .get(language.toLowerCase())
                 .write().string(content);
    }
    
    public static void deleteTranslation(ContentObject contentObject, String language) {
        contentObject.value()
                .get(language.toLowerCase())
                .delete();
    }
    
    public static void updateCmsObjectsByCurrentSelected(List<CMSObject> cmsObjects, CMSObject currentSelected) {
        boolean result = cmsObjects.removeIf(cmsObject -> StringUtils.equals(cmsObject.getPath(), currentSelected.getPath()));
        if(result) {
            cmsObjects.add(currentSelected);
        }
    }
}
