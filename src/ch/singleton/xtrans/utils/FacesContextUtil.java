package ch.singleton.xtrans.utils;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.PrimeFaces;

public final class FacesContextUtil {
    
    private FacesContextUtil() {
    }
    
    /**
     * Get External Context.
     * @return externalContext
     */
    public static ExternalContext getExternalContext() {
        return FacesContext.getCurrentInstance().getExternalContext();
    }
    
    public static void updateComponent(String commnand) {
        PrimeFaces.current().ajax().update(commnand);
    }
    
    public static void addMessage(String summary) {
        FacesMessage message = new FacesMessage("Successful", summary);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}
