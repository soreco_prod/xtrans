package ch.singleton.xtrans.utils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import ch.ivyteam.ivy.cm.ContentObject;
import ch.ivyteam.ivy.cm.ContentObjectType;
import ch.xpertline.xtrans.CMSObject;
import ch.xpertline.xtrans.CMSTranslator;

public final class CmsTableFactory {
    
    private CmsTableFactory() {
    }
    
    public static List<CMSObject> buildTableByNode(String uri, CMSTranslator cmsTranslator) {
        return cmsTranslator.getCms().get(uri)
                .map(contentObject -> cmsTranslator.getContentHierarchy(contentObject))
                .map(list -> cmsTranslator.getWholeContent(list))
                .orElse(Collections.emptyList());
    }
    
    public static List<CMSObject> buildTableByUriListAndType(List<String> uriList, ContentObjectType type, CMSTranslator cmsTranslator) {
        return uriList
                .stream()
                .map(uri -> buildTableByUriAndType(uri, type, cmsTranslator))
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }
    
    public static List<CMSObject> buildTableByUriAndType(String uri, ContentObjectType type, CMSTranslator cmsTranslator) {
        return cmsTranslator.getCms().get(uri)
                .map(contentObject -> cmsTranslator.getContentHierarchy(contentObject))
                .map(list -> cmsTranslator.getWholeContent(list))
                .orElse(Collections.emptyList())
                .stream()
                .filter(cmsObject -> cmsObject.getType() == type)
                .collect(Collectors.toList());
    }
    
    public static List<CMSObject> buildFullTableByType(ContentObjectType type, CMSTranslator cmsTranslator) {
        ContentObject rootObject = cmsTranslator.getCms().root();
        return Optional.of(cmsTranslator.getContentHierarchy(rootObject))
                .map(list -> cmsTranslator.getWholeContent(list))
                .orElse(Collections.emptyList())
                .stream()
                .filter(cmsObject -> cmsObject.getType() == type)
                .distinct()
                .collect(Collectors.toList());
    }
    
    public static List<CMSObject> buildFullTableByType(String uri, ContentObjectType type, CMSTranslator cmsTranslator) {
        return cmsTranslator.getCms().get(uri)
                .map(contentObject -> cmsTranslator.getContentHierarchy(contentObject))
                .map(list -> cmsTranslator.getWholeContent(list))
                .orElse(Collections.emptyList())
                .stream()
                .filter(cmsObject -> cmsObject.getType() == type)
                .distinct()
                .collect(Collectors.toList());
    }

}

