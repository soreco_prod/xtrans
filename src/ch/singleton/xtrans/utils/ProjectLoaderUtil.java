package ch.singleton.xtrans.utils;

import java.util.List;
import java.util.stream.Collectors;

import ch.ivyteam.ivy.application.IApplication;
import ch.ivyteam.ivy.application.IProcessModel;
import ch.ivyteam.ivy.environment.Ivy;

public final class ProjectLoaderUtil {
    
    private ProjectLoaderUtil() {
    }
    
    public static List<String> buildProjectNameList() {
        IApplication application =  IApplication.current();
        List<IProcessModel> models = application.getProcessModelsSortedByName();
        return CollectionUtil.getOrEmpty(models)
                .stream()
                .map(model -> model.getName())
                .collect(Collectors.toList());
    }

}
