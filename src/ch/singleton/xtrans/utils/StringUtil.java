package ch.singleton.xtrans.utils;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

public final class StringUtil {
    private StringUtil() {
    }

    public static final char CHAR_SPACE = ' ';
    public static final char UNIX_PATH_SEPARATOR = '/';
    public static final String COMMA = ",";
    public static final String SPACE = " ";
    public static final String DOT = ".";
    public static final String COMMA_WITH_SPACE = ", ";
    public static final String[] EMPTY_ARRAY = new String[0];
    public static final String COLON = ":";
    public static final String TASKLIST_FILTER_SETTING = "TASKLIST_FILTER_SETTING";
    public static final String ZERO_PERCENT = "0%";
    public static final String HYPHEN_WITH_SPACE = "%s - %s";
    public static final String CONCAT_WITH_DOT = "%s.%s";
    public static final String FULL_NAME_AND_USER_NAME_FORMAT = "%s (%s)";
    public static final String ROLE_DESCRIPTION_AND_ROLE_NAME = "%s (%s)";
    public static final String USER_FULL_NAME_AND_ROLE_DESCRIPTION_FORMAT = "%s (%s)";
    public static final String UNDEFINED_ERROR = "UNDEFINED_ERROR";
    public static final String HYPHEN = "-";
    public static final String INDIVIDUAL_FIELD_FORMAT = "Individual_%s";
    public static final String ZERO_STRING = "0";
    public static final String YES = "YES";
    
    /**
     * @param value : String
     * @return empty string if @value is null.
     * Trim whitespace at the beginning and end of @value.
     */
    public static String trimOrEmpty(String value) {
        return Optional.ofNullable(value)
                .map(String::trim)
                .orElse(EMPTY);
    }

    /**
     * @param rawValue : String
     * @return wrap method FilenameUtils.normalize(), return empty if sanitize happen.
     * Sanitize filename path user input.
     */
    public static String normalize(String rawValue) {
        String value = trimOrEmpty(rawValue);
        if (value.contains(String.valueOf(UNIX_PATH_SEPARATOR))) {
            return trimOrEmpty(FilenameUtils.normalize(value, true));
        }
        return trimOrEmpty(FilenameUtils.normalize(value, false));
    }


    /**
     * Merge array of string to one string and separate by ' - '.
     * Example:
     * param: values["value1","value2"] -> result: "value1 - value2"
     *
     * @param values arrays of string
     * @return one string
     */
    public static String mergeStringSeparateByHyphen(String... values) {
        String[] newValues = Stream.of(values)
                .filter(StringUtils::isNoneEmpty)
                .toArray(String[]::new);

        if (newValues.length > 1) {
            return Stream.of(newValues).collect(Collectors.joining(" - "));
        }
        return Stream.of(newValues).collect(Collectors.joining(EMPTY));
    }

    /**
     * Automatically add double backslash \\ if value contain special character
     * <([{\^-=$!|]})?*+.>
     *
     * @param value :String
     * @return new value with double backslash before special character
     */
    public static String autoAddEscapeSymbol(String value) {
        return value.replaceAll("[\\<\\(\\[\\{\\\\\\^\\-\\=\\$\\!\\|\\]\\}\\)‌​\\?\\*\\+\\.\\>]", "\\\\$0");
    }
    
    /**
     * Get optional of text.
     * If text is blank will return Optional.empty.
     * @param text the text
     * @return Optional of text
     */
    public static Optional<String> getOptionalOrEmpty(String text) {
        return Optional.ofNullable(text)
                       .filter(StringUtils::isNotBlank);
    }
    
    /**
     * Remove the colon at the end of string.
     * @param label
     * @return label that remove the colon ending
     */
    public static String removeLastColon(String label){
        if (StringUtils.isBlank(label)) {
            return EMPTY;
        } else {
            return label.replaceFirst(":$","");
        }
    }
    
    public static String getOrNull(String text) {
        if (StringUtils.isBlank(text)) {
            return null;
        }
        return text;
    }
    
    public static boolean isNullOrEmpty(String string) {
        return Objects.isNull(string) || string.isEmpty();
    }
    
    public static boolean compareIgnoreCase(String firstVal,String secondVal) {
        return trimOrEmpty(firstVal).equalsIgnoreCase(trimOrEmpty(secondVal));
    }
    
    public static String trimToLowerCase(String val) {
        return trimOrEmpty(val).toLowerCase();
    }
    
    public static boolean compareAcceptNull(Object firstVal, Object secondVal) {
        return parseToStringOrGetEmpty(firstVal).equals(parseToStringOrGetEmpty(secondVal));
    }
    
    public static String parseToStringOrGetEmpty(Object object) {
        return Optional.ofNullable(object).map(value -> value.toString()).orElseGet(() -> StringUtils.EMPTY);
    }
    
    /**
     * Remove space, underscore and lowercase.
     * @param label
     * @return label that space, underscore and lowercase
     */
    private static String removeSpaceUnderscoreLowercase(String label){
        if (StringUtils.isBlank(label)) {
            return EMPTY;
        } else {
            return label.replace(" ","").replace("_","").toLowerCase();
        }
    }
    
    /**
     * Compare 2 strings ignore space, underscore, case.
     * @param firstVal
     * @param secondVal
     * @return
     */
    public static boolean equalsIgnoreCaseSpaceUnderscore(String firstVal, String secondVal) {
        return removeSpaceUnderscoreLowercase(firstVal).equals(removeSpaceUnderscoreLowercase(secondVal));
    }
    
    /**
     * Join the list of String by the comma.
     * Example: ["A","B","C","D"] -> "A,B,C,D"
     * @param strings list of strings
     * @return list of strings as a string
     */
    public static String join(List<String> strings) {
        if (CollectionUtil.isNullOrEmpty(strings)) {
            return EMPTY;
        }
        return String.join(COMMA, strings);
    }
    
    public static String join(List<String> values, String seperator) {
        if (CollectionUtil.isNullOrEmpty(values)) {
            return EMPTY;
        }
        return StringUtils.join(values, seperator);
    }
    
    /**
     * Split the list of String by the comma.
     * Example: "A,B,C,D" -> ["A","B","C","D"]
     * @param list of strings as a string
     * @return strings list of strings
     */
    public static List<String> split(String value) {
        return Arrays.asList(Optional.ofNullable(value)
                .filter(StringUtils::isNoneBlank)
                .map(str -> str.split(COMMA))
                .orElse(EMPTY_ARRAY));
    }
    
    /**
     * Remove non numeric characters out of input string.
     * It is used to get the itemLine index from toogleClientId
     * Ex: 
     * accountingForm:accountingPanel:itemLineTable:0:itemLineItemRequestTablePanel -> 0
     * accountingForm:accountingPanel:itemLineTable:1:itemLineItemRequestTablePanel -> 1
     *  
     * @param str
     * @return the string that removed non numeric characters
     */
    public static String removeNonNumericCharacters(String str) {
        if (StringUtils.isBlank(str)) {
            return EMPTY;
        }
        return str.replaceAll("[^\\d.]", "");
    }
    
    /**
     * Compare 2 strings after trim.
     *  
     * @param str1 the first string
     * @param str2 the second string
     * @return true if 2 strings match.
     */
    public static boolean equalsWithTrim(String str1, String str2) {
        return trimOrEmpty(str1).equals(trimOrEmpty(str2));
    }
    
    /**
     * Check the input contains search string case insensitive.
     * @param input The string to be check
     * @param searchString The search string
     * @return boolean value
     */
    public static boolean containsCaseInsensitive(String input, String searchString) {
        return trimToLowerCase(input).contains(trimToLowerCase(searchString));
    }
    
    /**
     * Trim whitespace at the beginning and end of @value.
     * @param value : String
     * @return null if @value is null.
     */
    public static String trimOrNull(String value) {
        return Optional.ofNullable(value)
                .map(String::trim)
                .orElse(null);
    }
    
    /**
     * Decode string follow base64.
     * @param value input.
     * @return String after decode.
     */
    public static String base64Decode(String value){
        byte[] valueDecoded = Base64.getDecoder().decode(value);
        return new String(valueDecoded);
    }
    
    /**
     * Encode string follow base64.
     * @param value input.
     * @return String after encode.
     */
    public static String base64Encode(String value){
        return Base64.getEncoder().encodeToString(value.getBytes());
    }
    
    /**
     * Find duplicated values from string list.
     * @param listContainingDuplicates.
     * @return duplicate values.
     */
    public static List<String> findDuplicatesFromList(List<String> listContainingDuplicates) {
        final List<String> duplicateValues = new ArrayList<>();
        final Set<String> tempSet = new HashSet<>();
        for (String value : listContainingDuplicates) {
            if (!tempSet.add(value)) {
                duplicateValues.add(value);
            }
        }
        return duplicateValues;
    }
    
    public static String appendHyphen(String text1, String text2) {
        return String.format(HYPHEN_WITH_SPACE, text1, text2);
    }
    
    /**
     * Compare 2 strings ignore case after trim.
     *  
     * @param str1 the first string
     * @param str2 the second string
     * @return true if 2 strings match.
     */
    public static boolean equalsWithTrimIgnoreCase(String str1, String str2) {
        return StringUtils.equalsIgnoreCase(trimOrEmpty(str1), trimOrEmpty(str2));
    }

    /**
     * Split string into list, separated by comma.
     * @param text The string to be slit.
     * @return list of substring
     */
    public static List<String> splitStringToList(String text) {
        if(StringUtils.isEmpty(text)) {
            return Collections.emptyList();
        }
        
        return Arrays.asList(text.split(COMMA))
                .stream()
                .filter(StringUtils::isNotBlank)
                .map(String::trim)
                .collect(Collectors.toList());
    }
    
    /**
     * Get default if text input is blank.
     * @param textInput input.
     * @param defaultValue input.
     * @return result.
     */
    public static String defaultIfBlank(String textInput, String defaultValue) {
        if(StringUtils.isBlank(textInput)) {
            return defaultValue;
        }
        return textInput;
    }
    
    /**
     * Check if is blank will be return empty otherwise return input text.
     * @param text input.
     * @return value.
     */
    public static String getOrEmpty(String text) {
        if (StringUtils.isBlank(text)) {
            return StringUtils.EMPTY;
        }
        return text;
    }
    
}
