package ch.singleton.xtrans.utils;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import ch.ivyteam.ivy.cm.ContentObject;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.security.ISecurityContext;

public final class IvyWfUtil {

    
    /**
     * Get Session folder of Ivy application.
     * @return Path of folder
     */
    public static Path getSessionFileArea() {
        return Ivy.getInstance().request.getApplication().getSessionFileArea().toPath();
    }
    
    /**
     * Get Store folder of Ivy application.
     * @return path of folder.
     */
    public static Path getFileArea() {
        return Ivy.getInstance().request.getApplication().getFileArea().toPath();
    }

    /**
     * Get Ivy security context.
     * It is used in eapf_administration. TestDataUtil.deleteTestData
     * @return Ivy security context
     */
    public static ISecurityContext getSecurityContext() {
        return Ivy.wf().getSecurityContext();
    }
    
    public static List<String> getLanguageCodes() {
        return CollectionUtil.getOrEmpty(Ivy.cms().locales())
                .stream()
                .map(local -> local.getISO3Country())
                .collect(Collectors.toList());
    }
    
    public static List<ContentObject> getCmsByProjectName(String projectName) {
        return Ivy.cms().get(projectName).map(cms -> cms.children()).orElse(Collections.emptyList());
    }
}
