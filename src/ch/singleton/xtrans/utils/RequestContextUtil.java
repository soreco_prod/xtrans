package ch.singleton.xtrans.utils;

import java.util.Objects;

import org.primefaces.PrimeFaces;

import ch.ivyteam.ivy.environment.Ivy;

public final class RequestContextUtil {

    /**
     * Execute the command.
     * If the request context is not initialized -> DO NOTHING.
     * @param command command to execute
     */
    public static void execute(String command) {
        PrimeFaces primeFaces = PrimeFaces.current();
        if (Objects.nonNull(primeFaces)) {
            primeFaces.executeScript(command);
        } else {
            Ivy.log().warn(String.format("Request context is not initialized for executing command: {0}", command));
        }
    }
}