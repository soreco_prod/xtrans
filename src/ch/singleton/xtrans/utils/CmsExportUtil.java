package ch.singleton.xtrans.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Workbook;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;

import ch.ivyteam.ivy.cm.ContentObjectType;
import ch.ivyteam.ivy.environment.Ivy;
import ch.xpertline.xtrans.CMSObject;
import ch.xpertline.xtrans.CMSTranslator;
import ch.xpertline.xtrans.TreeData;


public final class CmsExportUtil {
    
    private static List<String> uriList;
    private CmsExportUtil() {}
    
    public static final String SHEET_NAME = "CMS";
    public static final Integer MAX_RECORDS = 20000;
    public static final Integer COLUMN_WIDTH = 15;
    
    private static final String FILE_NAME = SHEET_NAME + "_export_%s-%s.xls";
    private static final String COULD_NOT_WRITE_CMS_LIST_ERROR = "Could not write cms object list to directory {}";
    private static final String FILE_NOT_FOUND_ERROR = "File could not be found {}";
    
    
    /**
     * Build StreamedContent which sent to client.
     * @param workbook
     * @param projectName 
     * @return StreamedContent
     */
    public static StreamedContent buildStreamContent(Workbook workbook, String projectName) {
        String fileName = prepareFileName(projectName);
        Path exportFilePath = IvyWfUtil.getSessionFileArea().resolve(fileName);
        File exportFile = new File(exportFilePath.toString());
        try (FileOutputStream outFile = new FileOutputStream(exportFile)){
            workbook.write(outFile);
        } catch (IOException e) {
            Ivy.log().error(COULD_NOT_WRITE_CMS_LIST_ERROR, exportFilePath.toString(), e);
            return null;
        }
        String contentType = DocumentUtil.getContentType(fileName);
        return Optional.ofNullable(FileInputStreamUtil.generateFileInputStream(exportFile, FILE_NOT_FOUND_ERROR, exportFilePath.toString()))
                .map(fis -> DefaultStreamedContent.builder().stream(() -> fis).contentType(contentType).name(fileName).build())
                .orElse(null);
    }
    
    private static String prepareFileName(String projectName) {
        return String.format(FILE_NAME, projectName, System.currentTimeMillis());
    }
    
    public static List<CMSObject> buildFullCmsObjectsForProject(CMSTranslator cmsTranslator, List<String> languages, TreeNode<TreeData> root) {
        if(uriList == null) {
            uriList = new ArrayList<>();
        } else {
            uriList.clear();
        }
        visitAllNodes(root);
        return CollectionUtil.getOrEmpty(new ArrayList<>(uriList))
                .stream()
                .map(uri ->  CmsTableFactory.buildFullTableByType(uri, ContentObjectType.STRING,cmsTranslator))
                .flatMap(List::stream)
                .distinct()
                .collect(Collectors.toList());
    }
    
    public static void visitAllNodes(TreeNode<TreeData> node) {
        String uri = node.getData().getUri();
        if(!uriList.contains(uri)) {
            uriList.add(uri);
        }
        if (node.getChildCount() >= 0) {
            Iterator<TreeNode<TreeData>> iterator = node.getChildren().iterator();
            while(iterator.hasNext()) {
                TreeNode<TreeData> child = iterator.next();
                visitAllNodes(child);
            }
        }
    }
}

