package ch.singleton.xtrans.utils;

import java.io.File;
import java.io.FileInputStream;

import ch.ivyteam.ivy.environment.Ivy;

public final class FileInputStreamUtil {

	public FileInputStreamUtil() {}
	
    /**
     * Build StreamedContent which sent to client.
     * @param file file to transfer
     * @param errorMsg
     * @param path path of file
     * @return FileInputStream
    */
    public static FileInputStream generateFileInputStream(File file, String errorMsg, String path) {
        try {
            return new FileInputStream(file);
        } catch (Exception e) {
            Ivy.log().error(errorMsg, path, e);
            return null;
        }
    }
}
