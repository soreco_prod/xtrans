package ch.singleton.xtrans.utils;

import javax.faces.context.ExternalContext;

import org.apache.commons.lang3.StringUtils;


public final class DocumentUtil {
	
	private DocumentUtil() {
	}

    /**
     * Get type of content.
     * @param fileName input.
     * @return content type.
     */
    public static String getContentType(String fileName) {
        if (StringUtils.isBlank(fileName)){
            return StringUtils.EMPTY;
        }
        
        ExternalContext ec = FacesContextUtil.getExternalContext();
        String contentType = ec.getMimeType(fileName);
        return contentType;
    }
}
