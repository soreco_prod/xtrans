package ch.singleton.xtrans.utils;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import ch.xpertline.xtrans.CMSObject;


public final class CmsImportUtil {
    
    private CmsImportUtil() {}
    
    public static List<CMSObject> extractObjectsWithDifferentValue(List<CMSObject> cmsFromProject, List<CMSObject> importedCmsObjectsFromExcel) {
        
        List<String> importedUriList = CollectionUtil.getOrEmpty(importedCmsObjectsFromExcel)
                .stream()
                .map(CMSObject::getPath)
                .collect(Collectors.toList());
        
        return CollectionUtil.getOrEmpty(cmsFromProject)
                .stream()
                .filter(cmsObject -> filterNotEqualValuesFromImportCmsObjects(cmsObject, importedUriList, importedCmsObjectsFromExcel))
                .map(cmsObject -> {
                    extractValueByPath(cmsObject.getPath(), importedCmsObjectsFromExcel)
                        .ifPresent(value -> cmsObject.setValue(value));
                    return cmsObject;
                })
                .distinct()
                .collect(Collectors.toList());
    }
    
    private static boolean compareValues(CMSObject cmsObject, List<CMSObject> importedCmsObjectsFromExcel) {
        return extractValueByPath(cmsObject.getPath(), importedCmsObjectsFromExcel)
                .map(extractedValue -> isNotEqual(cmsObject.getValue(), extractedValue))
                .orElse(false);
    }
    
    private static boolean isNotEqual(Map<String, String> valueToCompare, Map<String, String> extractedValue) {
        return !CollectionUtil.getOrEmpty(valueToCompare).entrySet()
                .stream()
                .allMatch(e -> e.getValue().equals(extractedValue.get(e.getKey())));
    }
    
    private static Optional<Map<String, String>> extractValueByPath(String path, List<CMSObject> cmsObjects) {
        return CollectionUtil.getOrEmpty(cmsObjects)
                .stream()
                .filter(cmsObject -> StringUtils.equals(cmsObject.getPath(), path))
                .findAny()
                .map(CMSObject::getValue);
    }
    
    private static boolean filterNotEqualValuesFromImportCmsObjects(CMSObject cmsObject, List<String> importedUriList, List<CMSObject> importedCmsObjectsFromExcel) {
        return importedUriList.contains(cmsObject.getPath()) && 
                compareValues(cmsObject, importedCmsObjectsFromExcel);
    }

}
