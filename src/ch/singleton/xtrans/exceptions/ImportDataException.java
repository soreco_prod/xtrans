package ch.singleton.xtrans.exceptions;

public class ImportDataException  extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ImportDataException() {
    }
    
    public ImportDataException(String message) {
        super(message);
    }
}
