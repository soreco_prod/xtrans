package ch.xpertline.xtrans.excel;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import ch.ivyteam.ivy.cm.ContentObjectType;
import ch.ivyteam.ivy.environment.Ivy;
import ch.singleton.xtrans.utils.CmsLocaleUtil;
import ch.xpertline.xtrans.CMSObject;
import ch.xpertline.xtrans.CMSTranslator;


public class ImportHandler {

    public List<CMSObject> getCmsObjectList() {
        return cmsObjectList;
    }

    private List<CMSObject> cmsObjectList;
    private List<String> languagesSupported;
    private List<String> importedlanguages;
    
    public ImportHandler(CMSTranslator cmsTranslator) {
        this.cmsObjectList = new ArrayList<>();
        this.importedlanguages = new ArrayList<>();
        this.languagesSupported = CmsLocaleUtil.buildSupportedLanguageCodeList(cmsTranslator);
    }
    
    public void readExcelDataFromFile(InputStream inputStream) {
        try {
            //Create Workbook instance holding reference to .xlsx file
            HSSFWorkbook wb = new HSSFWorkbook(inputStream);
 
            //Get first/desired sheet from the workbook
            HSSFSheet ws = wb.getSheetAt(0);
 
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = ws.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if(row.getRowNum() == 0) {
                    for(int i=2; i < this.languagesSupported.size() + 2; i++) {
                        Cell languageCell = row.getCell(i);
                        String language = languageCell.getStringCellValue();
                        if(this.languagesSupported.contains(language)) {
                            this.importedlanguages.add(language);
                        }
                    }
                    continue;
                }
                
                Cell nameCell = row.getCell(0);
                String name = nameCell.getStringCellValue();
                Cell pathCell = row.getCell(1);
                String path = pathCell.getStringCellValue();
                Map<String, String> values = new HashMap<>();
                for(int i=2; i < this.importedlanguages.size() + 2; i++) {
                    Cell valueCell = row.getCell(i);
                    if(valueCell == null) {
                        continue;
                    }
                    String value = valueCell.getStringCellValue();
                    values.put(this.importedlanguages.get(i - 2), value);
                }
                CMSObject cmsObjectImported = new CMSObject(name, path, null, ContentObjectType.STRING, values);
                this.cmsObjectList.add(cmsObjectImported);
            }
            wb.close();
        } catch (Exception ex) {
            Ivy.log().error(ex);
        }
    }
}
