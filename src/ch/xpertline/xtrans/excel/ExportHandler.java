package ch.xpertline.xtrans.excel;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.primefaces.model.TreeNode;

import ch.ivyteam.ivy.cm.ContentObjectType;
import ch.ivyteam.ivy.environment.Ivy;
import ch.singleton.xtrans.utils.CmsExportUtil;
import ch.singleton.xtrans.utils.CmsLocaleUtil;
import ch.singleton.xtrans.utils.CmsTableFactory;
import ch.xpertline.xtrans.CMSObject;
import ch.xpertline.xtrans.CMSTranslator;
import ch.xpertline.xtrans.TreeData;


public final class ExportHandler {
    
    public ExportHandler() {
    }
    
    public static Workbook buildExcelExport(String projectName, List<String> uriList, CMSTranslator cmsTranslator) throws Exception {
        Ivy.log().info(projectName);
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet(CmsExportUtil.SHEET_NAME);
        sheet.setDefaultColumnWidth(CmsExportUtil.COLUMN_WIDTH);
        List<String> languages = CmsLocaleUtil.buildSupportedLanguageCodeList(cmsTranslator);
        buildHeaderRow(sheet, cmsTranslator, languages);
        buildContentOfCms(uriList, cmsTranslator, sheet, workbook, languages);
        return workbook;
    }
    
    public static Workbook buildFullExcelExport(String projectName, TreeNode<TreeData> root) throws Exception {
        Ivy.log().info(projectName);
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet(CmsExportUtil.SHEET_NAME);
        sheet.setDefaultColumnWidth(CmsExportUtil.COLUMN_WIDTH);
        CMSTranslator cmsTranslator = new CMSTranslator(projectName);
        List<String> languages = CmsLocaleUtil.buildSupportedLanguageCodeList(cmsTranslator);
        buildHeaderRow(sheet, cmsTranslator, languages);
        buildFullContentOfCms(cmsTranslator, sheet, workbook, languages, root);
        return workbook;
    }
    
    private static void buildHeaderRow(Sheet sheet, CMSTranslator cmsTranslator, List<String> languages) {
        List<String> headerRowItems = new ArrayList<>();
        headerRowItems.add("Name");
        headerRowItems.add("URI");
        headerRowItems.addAll(languages);
        Row row = sheet.createRow(0);
        for (int i = 0; i < headerRowItems.size(); i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(headerRowItems.get(i).toString());
        }
    }
        
    private static void buildContentOfCms(List<String> uriList, CMSTranslator cmsTranslator, Sheet sheet, Workbook workbook, List<String> languages) {
        List<CMSObject> cmsObjectList = CmsTableFactory.buildTableByUriListAndType(uriList, ContentObjectType.STRING, cmsTranslator);
        CellStyle cs = workbook.createCellStyle();
        for (int i = 0; i < cmsObjectList.size(); i++) {
            Row r = sheet.createRow(i + 1);
            CMSObject cmsObject = cmsObjectList.get(i);
            Cell column1;
            column1 = r.createCell(0);
            column1.setCellValue(cmsObject.getName());
            column1.setCellStyle(cs);
            
            Cell column2;
            column2 = r.createCell(1);
            column2.setCellValue(cmsObject.getPath());
            column2.setCellStyle(cs);
            
            for (int j = 2; j < languages.size() + 2; j++) {
                Cell columnx;
                columnx = r.createCell(j);
                columnx.setCellValue(cmsObject.getValue().get(languages.get(j - 2)));
                columnx.setCellStyle(cs);
            }
        }
        for (int z = 0; z < languages.size() + 2; z++) {
            sheet.autoSizeColumn(z);
        }
    }
    
    private static void buildFullContentOfCms(CMSTranslator cmsTranslator, Sheet sheet, Workbook workbook, List<String> languages, TreeNode<TreeData> root) throws Exception {
        CellStyle cs = workbook.createCellStyle();
        List<CMSObject> cmsObjectList = CmsExportUtil.buildFullCmsObjectsForProject(cmsTranslator, languages, root);

        for (int i = 0; i < cmsObjectList.size(); i++) {
            Row r = sheet.createRow(i + 1);
            CMSObject cmsObject = cmsObjectList.get(i);
            Cell column1;
            column1 = r.createCell(0);
            column1.setCellValue(cmsObject.getName());
            column1.setCellStyle(cs);
            
            Cell column2;
            column2 = r.createCell(1);
            column2.setCellValue(cmsObject.getPath());
            column2.setCellStyle(cs);
            
            for (int j = 2; j < languages.size() + 2; j++) {
                Cell columnx;
                columnx = r.createCell(j);
                if(cmsObject.getValue() == null) {
                    continue;
                }
                columnx.setCellValue(cmsObject.getValue().get(languages.get(j - 2)));
                columnx.setCellStyle(cs);
            }
        }
        for (int z = 0; z < languages.size() + 2; z++) {
            sheet.autoSizeColumn(z);
        }
    }
}

