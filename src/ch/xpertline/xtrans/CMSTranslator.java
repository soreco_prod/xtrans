package ch.xpertline.xtrans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import ch.ivyteam.ivy.application.IProcessModel;
import ch.ivyteam.ivy.cm.ContentManagementSystem;
import ch.ivyteam.ivy.cm.ContentObject;
import ch.ivyteam.ivy.cm.ContentObjectType;
import ch.ivyteam.ivy.cm.ContentObjectValue;
import ch.ivyteam.ivy.cm.exec.ContentManagement;
import ch.ivyteam.ivy.environment.Ivy;

import ch.singleton.xtrans.utils.CollectionUtil;
import ch.singleton.xtrans.utils.StringUtil;


/**
 * Helper-class for translating CMS of other projects in the same workspace
 */
public class CMSTranslator {
    
    private ContentManagementSystem cms;
    
    /**
     * load a CMS and login as developer user
     * 
     * @param projectName
     * @throws Exception 
     */
    public CMSTranslator(String projectName) {
        Ivy.session().loginSessionUser("Developer", "Developer");
        this.cms = getCmForProjectNew(projectName);
    }
    
    public ContentManagementSystem getCms() {
        return this.cms;
    }

    private static ContentManagementSystem getCmForProjectNew(String _projectName) {
        IProcessModel processModel =Ivy.request().getApplication().getProcessModels()
            .stream()
            .filter(pm -> StringUtils.equalsIgnoreCase(_projectName, pm.getName()))
            .findAny()
            .orElse(null);
        return ContentManagement.cms(processModel.getReleasedProcessModelVersion());
    }
    
    public List<ContentObject> getContentHierarchy(ContentObject currentLevel) {
        List<ContentObject> allLevelChildren = new ArrayList<>();
        List<ContentObject> currentLevelChildren = currentLevel.children();
        if(CollectionUtil.isNullOrEmpty(currentLevelChildren)) {
            allLevelChildren.add(currentLevel);
            return allLevelChildren;
        }
        CollectionUtil.getOrEmpty(currentLevelChildren)
                .stream()
                .forEach(currentContentObject -> {
                    if (Constants.EDITABLE_CONTENT_OBJECT_TYPES.contains(currentContentObject.meta().contentObjectType())) {
                        allLevelChildren.addAll(getContentHierarchyList(currentContentObject));
                    } else if (currentContentObject.meta().contentObjectType() == ContentObjectType.FOLDER){
                        allLevelChildren.add(currentContentObject);
                    }
                });
        return allLevelChildren;
    }
    
    public List<ContentObject> getContentHierarchyList(ContentObject currentLevel) {
        List<ContentObject> allLevelChildren = new ArrayList<>();
        List<ContentObject> currentLevelChildren = currentLevel.children();
        
        // read current content object recursive and add all children to the output list
        currentLevelChildren.forEach(currentContentObject -> {
            if (Constants.EDITABLE_CONTENT_OBJECT_TYPES.contains(currentContentObject.meta().contentObjectType())) {
                allLevelChildren.addAll(getContentHierarchyList(currentContentObject));
            } else if (currentContentObject.meta().contentObjectType() == ContentObjectType.FOLDER){
                allLevelChildren.add(currentContentObject);
            }
        });
        return allLevelChildren;
    }
    
    public List<CMSObject> getWholeContent(List<ContentObject> contentObjectList) {
        List<CMSObject> translationObjectList = new ArrayList<>();
        CollectionUtil.getOrEmpty(contentObjectList)
                .forEach(currentContentObject -> {
                    CMSObject cmsObject = new CMSObject();
                    cmsObject.setName(currentContentObject.name());
                    cmsObject.setPath(currentContentObject.uri());
                    List<ContentObjectValue> currentObjectValues = currentContentObject.values();
                    ContentObjectType currentType = currentContentObject.meta().contentObjectType();
                    
                    if (currentObjectValues.size() > 0) {
                        HashMap<String, String> hm = new HashMap<String, String>();
                        cmsObject.setValue(hm);
                        for (ContentObjectValue currentObjectValue : currentObjectValues ) {
                            String language = StringUtils.EMPTY;
                            if (StringUtils.isNotBlank(currentObjectValue.locale().getDisplayCountry().trim())) {
                                language = currentObjectValue.locale().getLanguage().toUpperCase() + "_" + currentObjectValue.locale().getCountry(); 
                            } else {
                                language = currentObjectValue.locale().getLanguage().toUpperCase();
                            }
                            
                            if (Constants.EDITABLE_CONTENT_OBJECT_TYPES.contains(currentType)){
                                cmsObject.getValue().put(language, currentObjectValue.read().string());
                                cmsObject.setType(currentType);
                            } else {
                                cmsObject.setType(ContentObjectType.FOLDER);
                            }
                        }
                        this.addTextForSearch(cmsObject);
                    } else {
                        if (Constants.EDITABLE_CONTENT_OBJECT_TYPES.contains(currentType)){
                            cmsObject.setType(currentType);
                        } else {
                            cmsObject.setType(ContentObjectType.FOLDER);
                        }
                    }
                    translationObjectList.add(cmsObject);
                });
        return translationObjectList;
    }
    
    private void addTextForSearch(CMSObject cmsObject) {
        List<String> list = cmsObject.getValue().entrySet()
                .stream()
                .map(x -> x.getValue())
                .collect(Collectors.toList());
        cmsObject.setText(StringUtil.join(list));
    }
}
