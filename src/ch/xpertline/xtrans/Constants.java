package ch.xpertline.xtrans;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import ch.ivyteam.ivy.cm.ContentObjectType;

public class Constants {
    
    public final static List<ContentObjectType> EDITABLE_CONTENT_OBJECT_TYPES = Collections.unmodifiableList(Arrays.asList(
          ContentObjectType.STRING));
    
    
    public final static List<ContentObjectType> NOT_EDITABLE_CONTENT_OBJECT_TYPES = Collections.unmodifiableList(Arrays.asList(
          ContentObjectType.FOLDER, ContentObjectType.FILE));

}
