package ch.xpertline.xtrans;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import ch.ivyteam.ivy.cm.ContentObject;
import ch.ivyteam.ivy.cm.ContentObjectType;

public class CmsTree {
    
    private TreeNode<TreeData> main;

    public CmsTree() {
        this.main = new DefaultTreeNode<>(new TreeData("cms", "/"), null);
    }
    
    public CmsTree(List<ContentObject> contentObjectList) {
        generateTreeContentObject(contentObjectList);
    }
    
    public void setData(List<ContentObject> contentObjectList) {
        generateTreeContentObject(contentObjectList);
    }
    
    public TreeNode<TreeData> getTree(){
        return this.main;
    }
    
    private void generateTreeContentObject(java.util.List<ContentObject> coList) {
        for (int i = 0; i < coList.size(); i++) {
            ContentObjectType coType = coList.get(i).meta().contentObjectType(); 
            if(coType == ContentObjectType.FOLDER || Constants.EDITABLE_CONTENT_OBJECT_TYPES.contains(coType)) {
            	TreeData data = new TreeData(coList.get(i).name(), coList.get(i).uri());
                TreeNode<TreeData> sub = new DefaultTreeNode<>(data, this.main);
                sub.getChildren().addAll(addSubTree(coList.get(i)));
                this.main.getChildren().add(sub);
            }
        }
    }
    
    private List<TreeNode<TreeData>> addSubTree(ContentObject contentObject) {
        List<TreeNode<TreeData>> trees = new ArrayList<>();
        for (int j = 0; j < contentObject.children().size(); j++) {
            ContentObjectType coType = contentObject.children().get(j).meta().contentObjectType();
            if(coType == ContentObjectType.FOLDER || Constants.EDITABLE_CONTENT_OBJECT_TYPES.contains(contentObject.children().get(j).meta().contentObjectType())) {
            	TreeData data = new TreeData(contentObject.children().get(j).name(), contentObject.children().get(j).uri());
            	TreeNode<TreeData> sub = new DefaultTreeNode<>(data, this.main);
                List<TreeNode<TreeData>> childs = addSubTree(contentObject.children().get(j));
                if (childs.size() > 0 && !childs.isEmpty()){
                    sub.getChildren().addAll(childs);
                }
                trees.add(sub);
            }
        }
        return trees;
    }
}
