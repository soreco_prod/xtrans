package ch.xpertline.xtrans;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import ch.ivyteam.ivy.cm.ContentObjectType;

public class CMSObject {
    
    private String name;
    private String path;
    private String level;
    private ContentObjectType type;
    private Map<String, String> value;
    private String text;

    public CMSObject() {
    }
    
    public CMSObject(String name, String path, String level, ContentObjectType type, Map<String, String> value) {
        this.name = name;
        this.path = path;
        this.level = level;
        this.type = type;
        this.value = value;
        this.text = StringUtils.EMPTY;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }
    public String getLevel() {
        return level;
    }
    public void setLevel(String level) {
        this.level = level;
    }
    public ContentObjectType getType() {
        return type;
    }
    public void setType(ContentObjectType type) {
        this.type = type;
    }
    public Map<String, String> getValue() {
        return value;
    }
    public void setValue(Map<String, String> value) {
        this.value = value;
    }
    
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
